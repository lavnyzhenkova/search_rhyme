function fetchRhymes() {
    let word = document.getElementById('word').value;
    let syllable = document.getElementById('syllable').value;

    fetch(`/rhyme?word=${word}&syllable=${syllable}`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
    })
        .then(response => response.json())
        .then(data => displayRhymes(data))
        .catch(error => console.error('Error fetching rhymes:', error));
}

function displayRhymes(rhymes) {
    let rhymeListContainer = document.getElementById('rhymeList');
    rhymeListContainer.innerHTML = '<ul style="list-style-type:none;">' + rhymes.map(rhyme => '<li>' + rhyme + '</li>').join('') + '</ul>';
}