package ua.rhyme.model;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Getter
@Setter
@Document
@ToString(exclude = {"id", "phonetics", "stress"})
public class Word {
    @MongoId(FieldType.OBJECT_ID)
    private ObjectId id;

    private String word;

    private String phonetics;

    private int stress;

    public Word(String word, String phonetics, int stress) {
        this.word = word;
        this.phonetics = phonetics;
        this.stress = stress;
    }
}
