package ua.rhyme.model;

import com.mongodb.lang.Nullable;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Getter
@Setter
@Document
public class Synonym {
    @MongoId(FieldType.OBJECT_ID)
    private ObjectId id;
    private String word;
    @Nullable
    private List<String> similar;
}
