package ua.rhyme.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.rhyme.repository.SynonymRepository;

@Service
@RequiredArgsConstructor
public class SynonymService {
    private final SynonymRepository synonymRepository;
}
