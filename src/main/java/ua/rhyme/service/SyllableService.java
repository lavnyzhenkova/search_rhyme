package ua.rhyme.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SyllableService {

    public int countSyllables(String word) {
        char[] letters = word.toCharArray();
        int syllableCount = 0;

        for (int i = 0; i < letters.length; i++) {
            if (isVowel(letters[i]) && (i == 0 || !isVowel(letters[i - 1]))) {
                syllableCount++;
            }
        }

        if (word.endsWith("ий") || word.endsWith("ій")) {
            syllableCount--;
        }
        return syllableCount;
    }

    public static boolean isVowel(char letter) {
        return "АаЕеЄєИиІіОоУуЮюЯяЇїЁё".indexOf(letter) >= 0;
    }

    public String[] splitToSyllables(String word) {
        List<String> syllables = new ArrayList<>();
        StringBuilder syllableBuilder = new StringBuilder();

        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);

            if (isVowel(ch) && syllableBuilder.length() > 0) {
                syllableBuilder.append(ch);
                syllables.add(syllableBuilder.toString());
                syllableBuilder = new StringBuilder();
            } else if (i == word.length() - 1) {
                syllableBuilder.append(ch);
                syllables.add(syllableBuilder.toString());
            } else {
                syllableBuilder.append(ch);

                char nextCh = word.charAt(i + 1);
                char prevCh = i > 0 ? word.charAt(i - 1) : '\0';

                if (isMultipleConsonants(syllableBuilder.toString(), nextCh)) {
                    continue;
                } else if (ch == 'й' && (prevCh == 'л' || prevCh == 'м' || prevCh == 'н')) {
                    continue;
                } else if (ch == 'ь' || ch == 'ї') {
                    continue;
                } else if (isMultipleConsonants(syllableBuilder.toString())) {
                    int splitIndex = findConsonantSplitIndex(syllableBuilder.toString());
                    if (splitIndex != -1) {
                        syllables.add(syllableBuilder.substring(0, splitIndex));
                        syllableBuilder.delete(0, splitIndex);
                    }
                }
            }
        }

        return syllables.toArray(new String[0]);
    }

    private boolean isMultipleConsonants(String syllable) {
        int len = syllable.length();
        if (len < 2) {
            return false;
        }
        for (int i = 0; i < len - 1; i++) {
            char ch1 = syllable.charAt(i);
            char ch2 = syllable.charAt(i + 1);
            if (!isVowel(ch1) && !isVowel(ch2)) {
                return true;
            }
        }
        return false;
    }

    private boolean isMultipleConsonants(String syllable, char nextCh) {
        return isMultipleConsonants(syllable) && isMultipleConsonants(String.valueOf(nextCh));
    }

    private int findConsonantSplitIndex(String syllable) {
        for (int i = syllable.length() - 1; i >= 0; i--) {
            char ch = syllable.charAt(i);
            if (isVowel(ch)) {
                return i + 1;
            }
        }
        return -1;
    }
}



