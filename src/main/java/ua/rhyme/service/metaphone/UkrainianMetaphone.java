package ua.rhyme.service.metaphone;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import ua.rhyme.exception.WordNotFoundException;

@Getter
@Setter
@Service
public class UkrainianMetaphone {

    private int maxCodeLen = 4;
    private static final String VOWELS = "АОУІЕ";

    public String doubleMetaphone(final String value) throws WordNotFoundException {
        return doubleMetaphone(value, false);
    }

    public String doubleMetaphone(String value, final boolean alternate) throws WordNotFoundException {
        if (value == null) {
            throw new WordNotFoundException("No word for encryption");
        }

        int index = 0;
        char postChar = value.charAt(index + 1);
        final MetaphoneResult result = new MetaphoneResult(value.length());
        while (!result.isComplete() && index <= value.length() - 1) {
            if (postChar > value.length()) {
                postChar = value.charAt(index);
            }
            char ch = value.charAt(index);
            switch (ch) {
                case 'А', 'Є', 'Е', 'И', 'І', 'Ї', 'Ю', 'У', 'О', 'Я':
                    char vowel = VOWELS.charAt(indexOfVowels(ch));
                    result.append(vowel);
                    break;

                case 'Б', 'П':
                    result.append('Б');
                    break;

                case 'Т', 'Д':
                    if (postChar > value.length()) {
                        result.append('Т');
                        break;
                    }
                    if (ch == 'Д' && (postChar == 'З' || postChar == 'Ж')) {
                        break;
                    }
                    result.append('Т');

                    break;

                case 'З', 'Ц', 'С':
                    result.append('С');
                    break;

                case 'Ж', 'Ш', 'Щ', 'Ч':
                    result.append('Ш');
                    break;

                case 'Г', 'К', 'Х', 'Ґ':
                    result.append('Х');
                    break;

                case 'Л', 'Н', 'М', 'Р':
                    result.append('Л');
                    break;

                case 'В', 'Ф':
                    result.append('Ф');
                    break;

                case 'Ь', '’', '\'':
                    result.append('Ь');
                    break;

                case 'Й':
                    break;
                default:
                    throw new IllegalArgumentException("You must entered string in ukrainian language");
            }
            index++;
        }
        return alternate ? result.getAlternate() : result.getPrimary();
    }

    private int indexOfVowels(char ch) {
        if (ch == 'Я') {
            ch = 'А';
        }
        if (ch == 'Ю') {
            ch = 'У';
        }
        if (ch == 'Ї' || ch == 'И') {
            ch = 'І';
        }
        if (ch == 'Є') {
            ch = 'Е';
        }
        return VOWELS.indexOf(ch);
    }

    public class MetaphoneResult {
        private final StringBuilder primary = new StringBuilder(getMaxCodeLen());
        private final StringBuilder alternate = new StringBuilder(getMaxCodeLen());
        private final int maxLength;

        public MetaphoneResult(final int maxLength) {
            this.maxLength = maxLength;
        }

        public void append(final char value) {
            appendPrimary(value);
            appendAlternate(value);
        }

        public void append(final char primary, final char alternate) {
            appendPrimary(primary);
            appendAlternate(alternate);
        }

        public void appendPrimary(final char value) {
            if (this.primary.length() < this.maxLength) {
                this.primary.append(value);
            }
        }

        public void appendAlternate(final char value) {
            if (this.alternate.length() < this.maxLength) {
                this.alternate.append(value);
            }
        }

        public void append(final String value) {
            appendPrimary(value);
            appendAlternate(value);
        }

        public void append(final String primary, final String alternate) {
            appendPrimary(primary);
            appendAlternate(alternate);
        }

        public void appendPrimary(final String value) {
            final int addChars = this.maxLength - this.primary.length();
            if (value.length() <= addChars) {
                this.primary.append(value);
            } else {
                this.primary.append(value, 0, addChars);
            }
        }

        public void appendAlternate(final String value) {
            final int addChars = this.maxLength - this.alternate.length();
            if (value.length() <= addChars) {
                this.alternate.append(value);
            } else {
                this.alternate.append(value, 0, addChars);
            }
        }

        public String getPrimary() {
            return this.primary.toString();
        }

        public String getAlternate() {
            return this.alternate.toString();
        }

        public boolean isComplete() {
            return this.primary.length() >= this.maxLength &&
                    this.alternate.length() >= this.maxLength;
        }
    }
}
