package ua.rhyme.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ua.rhyme.exception.WordNotFoundException;
import ua.rhyme.service.metaphone.UkrainianMetaphone;
import ua.rhyme.model.Word;
import ua.rhyme.repository.WordRepository;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class RhymeService {

    private final WordRepository wordRepository;
    private final UkrainianMetaphone ukrainianMetaphone;
    private final SyllableService syllableService;
    List<String> rhymes = new ArrayList<>();
    Scanner scan = new Scanner(System.in);

    public List<String> findRhymes(List<String> words) {
        try {
            for (String word : words) {
                System.out.println(word + " : " + Arrays.toString(syllableService.splitToSyllables(word)));
                int stress = scan.nextInt();
                String cutWord = selectStress(word, stress);

                String encryptedWord = ukrainianMetaphone.doubleMetaphone(cutWord.toUpperCase()).substring(1);

                if (wordRepository.existsByWord(word)) {
                    continue;
                }

                wordRepository.save(new Word(word.toLowerCase(), encryptedWord, stress));

                rhymes.add(word);
            }
        } catch (WordNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        return rhymes;
    }

    public List<String> findRhymes(String word, int syllable) {

        String cutWord = selectStress(word, syllable);

        try {
            String encryptedWord = ukrainianMetaphone.doubleMetaphone(cutWord.toUpperCase()).substring(1);
            List<Word> words = wordRepository.findWordByPhonetics(encryptedWord);

            for (Word word1 : words) {
                rhymes.add(word1.getWord());
            }

            if (!wordRepository.existsByWord(word)) {
                wordRepository.save(new Word(word.toLowerCase(), encryptedWord, syllable));
            }

        } catch (WordNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        return rhymes;
    }

    //cut and return the word from the last syllable, where the vowel is stressed
    private String selectStress(String word, int syllable) {
        String[] toSyllables = syllableService.splitToSyllables(word);
        if (toSyllables.length == 1) {
            return String.join("", toSyllables);
        }
        return Arrays.stream(toSyllables)
                .skip(syllable - 1L)
                .collect(Collectors.joining());
    }
}
