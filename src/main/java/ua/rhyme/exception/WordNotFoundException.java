package ua.rhyme.exception;

public class WordNotFoundException extends Exception {
    public WordNotFoundException(String message) {
        super(message);
    }
}
