package ua.rhyme.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class ViewController {

    @GetMapping
    public ModelAndView getMainPage() {
        return new ModelAndView("index");
    }

    @GetMapping("/file-load")
    public ModelAndView loadFile() {
        ModelAndView modelAndView = new ModelAndView("file-load");

        return modelAndView;
    }

    @GetMapping("/rhyme")
    public ModelAndView getRhyme() {
        ModelAndView modelAndView = new ModelAndView("rhyme");

        return modelAndView;
    }

    @GetMapping("/synonym")
    public ModelAndView getSynonym() {
        ModelAndView modelAndView = new ModelAndView("synonym");

        return modelAndView;
    }
}
