package ua.rhyme.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import ua.rhyme.parser.FileScanner;
import ua.rhyme.service.RhymeService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class FileController {
    private final FileScanner fileScanner;
    private final RhymeService rhymeService;

    @PostMapping(value = "/file-load", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody ResponseEntity<List<String>> parseFile(@RequestParam("file") MultipartFile file) throws IOException {

        File convertFile = new File("src/main/resources/" + file.getOriginalFilename());
        convertFile.createNewFile();

        try (FileOutputStream fos = new FileOutputStream(convertFile)) {
            fos.write(file.getBytes());
        }

        List<String> parseFile = fileScanner.parseFile(convertFile);
        List<String> rhymes = rhymeService.findRhymes(parseFile);

        convertFile.delete();

        return rhymes.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(rhymes, HttpStatus.OK);
    }

}
