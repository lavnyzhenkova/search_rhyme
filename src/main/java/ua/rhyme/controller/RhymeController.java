package ua.rhyme.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.rhyme.service.RhymeService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RhymeController {
    private final RhymeService rhymeService;

    @PostMapping(value = "/rhyme", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> findRhymes(@RequestParam(name = "word") String word,
                                                   @RequestParam(name = "syllable", required = false, defaultValue = "1") int syllable) {
        List<String> rhymes = rhymeService.findRhymes(word, syllable);

        return rhymes.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(rhymes, HttpStatus.OK);
    }
}
