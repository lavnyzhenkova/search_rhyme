package ua.rhyme.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ua.rhyme.service.SynonymService;

@RestController
@RequiredArgsConstructor
public class SynonymController {
    private final SynonymService synonymService;
}
