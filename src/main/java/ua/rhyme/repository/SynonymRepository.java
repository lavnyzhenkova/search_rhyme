package ua.rhyme.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.rhyme.model.Synonym;

import java.util.List;

@Repository
public interface SynonymRepository extends MongoRepository<Synonym, ObjectId> {
    List<Synonym> findSynonymByWord(String word);
}
