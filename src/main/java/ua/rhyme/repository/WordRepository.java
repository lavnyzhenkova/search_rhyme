package ua.rhyme.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ua.rhyme.model.Word;

import java.util.List;

@Repository
public interface WordRepository extends MongoRepository<Word, ObjectId> {
    List<Word> findWordByPhonetics(String phonetics);
    boolean existsByWord(String word);

}
