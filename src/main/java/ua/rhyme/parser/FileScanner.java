package ua.rhyme.parser;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.google.common.base.CharMatcher;
import org.springframework.stereotype.Service;

@Getter
@Setter
@AllArgsConstructor
@Service
public class FileScanner {
    public List<String> parseFile(File file)  {
        List<String> wordsFromPoem = new ArrayList<>();

        try (FileReader fileReader = new FileReader(file)) {
            Scanner parser = new Scanner(fileReader);
            while (parser.hasNextLine()) {
                String line = parser.nextLine();

                String result = line.substring(line.lastIndexOf(" ") + 1).trim();
                String charsToRemove = "{[]}\",_!—.?–…:;.» ";
                result = CharMatcher.anyOf(charsToRemove).removeFrom(result);

                if (result.equals("")) {
                    continue;
                }
                wordsFromPoem.add(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wordsFromPoem;
    }
}
